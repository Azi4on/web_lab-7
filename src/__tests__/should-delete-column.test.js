import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { waitFor } from '@testing-library/react';
import { it, expect, jest } from '@jest/globals';
import App from '../App';

const COLUMN_TO_DELETE_NAME = 'Column to delete';
const DELETE_COLUMN_BUTTON_ID = 'Delete column';

jest.mock('../api', () => {
  return {
    api: {
      getColumns: async () => {
        return [{ name: COLUMN_TO_DELETE_NAME }];
      },
      getCurrentUser: async () => {
        return { name: 'John Doe' };
      },
      deleteColumn: async () => {
        return true;
      },
    },
  };
});

it(`В Column должна быть кнопка на удаление.
    По клику на нее колонка удаляется.`, async () => {
  const { findByText, queryByText } = render(<App />);

  const deleteButton = await findByText(DELETE_COLUMN_BUTTON_ID);
  fireEvent.click(deleteButton);

  await waitFor(() => expect(queryByText(COLUMN_TO_DELETE_NAME)).toBeFalsy());
});
